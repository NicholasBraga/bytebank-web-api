import 'package:bytebank/database/dao/contact_dao.dart';
import 'package:bytebank/models/contact.dart';
import 'package:flutter/material.dart';

class ContactForm extends StatefulWidget {
  @override
  _ContactFormState createState() => _ContactFormState();
}

class _ContactFormState extends State<ContactForm> {
  // Variáveis que serão usadas no widget de TextField para armazenar os dados digitados pelo usuário
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _accountNumberController =
      TextEditingController();
  final ContactDao _contactDao = ContactDao();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New contact"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            TextField(
              // Amarração entre o controller criado e o atributo do TextField para que ele salve o valor digitado na variável
              controller: _fullNameController,
              decoration: InputDecoration(
                labelText: "Full name",
              ),
              style: TextStyle(fontSize: 24.0),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
              ),
              child: TextField(
                // Amarração entre o controller criado e o atributo do TextField para que ele salve o valor digitado na variável
                controller: _accountNumberController,
                decoration: InputDecoration(
                  labelText: "Account number",
                ),
                style: TextStyle(
                  fontSize: 24.0,
                ),
                keyboardType: TextInputType.number,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
              ),
              child: SizedBox(
                width: double.maxFinite,
                child: RaisedButton(
                  child: Text("Create"),
                  onPressed: () {
                    final String name = _fullNameController.text; // Recupera o valor digitado pelo usuário e salva na variável
                    final int accountNumber =
                        int.tryParse(_accountNumberController.text); // Recupera o valor digitado pelo usuário e salva na variável
                    final Contact newContact = Contact(0, name, accountNumber); // Cria um objeto da classe "Contact"
                    _contactDao.save(newContact).then((id) => Navigator.pop(context));
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
