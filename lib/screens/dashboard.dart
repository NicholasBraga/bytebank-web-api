import 'package:bytebank/screens/contacts_list.dart';
import 'package:bytebank/screens/transactions_list.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
      ),
      body: Column(
        // O mainAxisAlignment é responsável pelo alinhamento vertical. O spaceBetween separou nos dois extremos da tela os conteúdos dentro do body
        mainAxisAlignment: MainAxisAlignment.spaceBetween,

        // O crossAxisAlignment é responsável pelo alinhamento horizontal. O start posicionou os elementos nos seus respectivos "Start", ou seja, do lado esquerdo
        crossAxisAlignment: CrossAxisAlignment.start,

        // children serve para quando se quer colocar widgets dentro de outro
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('images/bytebank_logo.png'),
          ),
          // O Container é o menu que contém os botões de navegação da dashboard
          Container(
            height: 110,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                _FeatureItem(
                  'Transfer',
                  Icons.monetization_on,
                  onClick: () {
                    _showContactsList(context);
                  },
                ),
                _FeatureItem(
                  'Transaction Feed',
                  Icons.description,
                  onClick: () {
                    _showTransactionsList(context);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  /**
   * Faz a navegação para a tela "ContactsList"
   */
  void _showContactsList(BuildContext context) {
    // Trecho de código para fazer a navegação de uma tela para outra
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => ContactsList(),
    ));
  }

  void _showTransactionsList(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => TransactionsList(),
    ));
  }
}

/**
 * Widget responsável por montar os botões na dashboard
 * @param name - nome do botão
 * @param icon - ícone do botão
 * @param onClick - função a ser chamada quando for clicada
 */
class _FeatureItem extends StatelessWidget {
  final String name;
  final IconData icon;
  final Function onClick;

  // As chaves ao redor do "this.onClick" servem para informar ao Flutter que trata-se de um parâmetro
  // opcional.
  // O "@required" servirá para que o IDE avise para quem for implementar
  // a classe "_FeatureItem" que o parâmetro precisa ser informado para que a classe funcione como esperado.
  // O "assert" garante que as variáveis não sejam do tipo informado, no caso, null
  _FeatureItem(this.name, this.icon, {@required this.onClick}) : assert(icon != null), assert(onClick != null);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Material(
          color: Theme.of(context).primaryColor,
          child: InkWell(
            // InkWell é o equivalente ao "GestureDetector" do Flutter, porém ele é dedicado ao Material. Ele permite colocar ações de movimentos em componentes que naturalmente nao possuem, como é o caso de agora, onde vamos atribuir o evento de "tap" em um container
            onTap: () {
              // Nesse caso, o "onClick()" é o atributo da classe. Serve para chamar as lógicas
              // de carregamento de tela
              onClick();
            },
            child: Container(
              padding: EdgeInsets.all(8),
              width: 150,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    // Icon recebido por parâmetro no construtor
                    icon,
                    color: Colors.white,
                    size: 24.0,
                  ),
                  Text(
                    // Name recebido por parâmetro no construtor
                    name,
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
