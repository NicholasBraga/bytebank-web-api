import 'package:http/http.dart';
import 'package:http_interceptor/http_interceptor.dart';

import 'interceptors/logging_interceptor.dart';

const String baseUrl =
    'https://infinite-harbor-30976.herokuapp.com/transactions';
/**
 * Interceptador para obtenção de feedback das requisições
 */
Client client = HttpClientWithInterceptor.build(
  interceptors: [LoggingInterceptor()],
);


