import 'dart:convert';

import 'package:bytebank/models/contact.dart';
import 'package:bytebank/models/transaction.dart';
import 'package:http/http.dart';

import '../webclient.dart';

class TransactionWebClient {

  Future<List<Transaction>> findAll() async {
    final Response response =
    await client.get(baseUrl).timeout(Duration(seconds: 30));

    List<Transaction> transactions = _toTransactions(response);
    return transactions;
  }

  Future<Transaction> save(Transaction transaction) async {
    // Para que possamos enviar o post, precisamos transformar o objeto "transaction"
    // em um objeto genérico, ou seja, Mapa com chaves "string" e valores dinamicos, que por sua vez,
    // será transformado em um json, estrutura de dado necessária para enviar no post
    Map<String, dynamic> transactionMap = _toMap(transaction);

    // json esperado pelo "body" da requisição
    final String transactionJson = jsonEncode(transactionMap);

    // O conteúdo do "headers" e do "body" são Maps. Eles estão declarados com as chaves {}
    final Response response = await client.post(baseUrl,
        headers: {
          'Content-type': 'application/json',
          'password': '1000',
        },
        body: transactionJson);

    // Decodificamos a response DE json PARA um objeto genérico (o Map de chave String e valores dinamicos)
    return _toTransaction(response);
  }

  List<Transaction> _toTransactions(Response response) {

    /// Decodifica o body da resposta e salva em uma lista cujo tipo de dado é dinamico
    final List<dynamic> decodedJson = jsonDecode(response.body);

    /// Lista de transações que serão retornadas pela função
    final List<Transaction> transactions = List();

    /// Iterando sobre cada valor do json decodificado e criamos
    /// uma estrutura MAP contendo uma chave String e valores dinâmicos (o "transactionJson),
    /// afinal, podem ser strings, inteiros, etc.
    /// A cada iteração, criamos uma instância de Transaction e Contact
    for (Map<String, dynamic> transactionJson in decodedJson) {
      /// Map criado apenas para evitar de chamar por "transactionJson['contact']['oAtributoDesejado']
      final Map<String, dynamic> contactJson = transactionJson['contact'];

      /// Instância da classe "Transaction" que está sendo criada
      final Transaction transaction = Transaction(
        transactionJson['value'],
        Contact(
          0,
          contactJson['name'],
          contactJson['accountNumber'],
        ),
      );

      /// Adicionando instâncias da classe "Transaction" a uma lista de instancias de "Transaction"
      transactions.add(transaction);
    }
    return transactions;
  }

  Transaction _toTransaction(Response response) {
    Map<String, dynamic> json = jsonDecode(response.body);

    // Extraimos em um objeto separado os dados do "Contato"
    final Map<String, dynamic> contactJson = json['contact'];

    // Retornamos um objeto do model "Transaction"
    return Transaction(
      json['value'],
      Contact(
        0,
        contactJson['name'],
        contactJson['accountNumber'],
      ),
    );
  }

  Map<String, dynamic> _toMap(Transaction transaction) {
    final Map<String, dynamic> transactionMap = {
      'value': transaction.value,
      'contact': {
        'name': transaction.contact.name,
        'accountNumber': transaction.contact.accountNumber,
      }
    };
    return transactionMap;
  }
}