import 'package:http_interceptor/http_interceptor.dart';

/**
 * Os interceptadores servem para que possamos saber sobre os dados das requisições
 * e respostas feitas pelo app através do protocolo HTTP
 * Como pode ser visto abaixo, é possível escolhermos quais os tipos de informações
 * queremos ter acesso.
 */
class LoggingInterceptor implements InterceptorContract {
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    print('***REQUEST***');
    print('Url: ${data.url}');
    print('Headers: ${data.headers}');
    print('Method: ${data.method}');
    print('Body: ${data.body}');
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    print('***RESPONSE***');
    print('Status Code: ${data.statusCode}');
    print('Headers: ${data.headers}');
    print('Method: ${data.method}');
    print('Body: ${data.body}');
    return data;
  }
}