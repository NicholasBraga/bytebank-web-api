import 'package:bytebank/database/dao/contact_dao.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

Future<Database> getDatabase() async {
  try {
    final String dbPath = await getDatabasesPath();
      /**
       * A função "join" vem da dependência "path" e serve para unir várias strings que são fornecidas como parâmetros para ela
       * Ela é útil pois permite que criemos o caminho para o sistema de arquivo de acordo com o sistema operacional (android ou iOS)
       * da forma correta
       */
      final String path = join(dbPath, 'bytebank.db');

      return openDatabase(
        path,
        onCreate: (db, version) {
          db.execute(ContactDao.tableSql);
        },
        version: 1,
        // Se houver um downgrade na versão do banco, apaga os dados do banco
//        onDowngrade: onDatabaseDowngradeDelete,
      );
  } catch (e, s) {
    print(s);
  }
}
